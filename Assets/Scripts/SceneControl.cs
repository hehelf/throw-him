﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public class SceneControl : MonoBehaviour
{   
    public GameObject Ball;
    public GameObject PhysBall;
    public float BallSize;
    public Vector2 PoolSize;
    public Transform Pool;
    public List<Animator> People = new List<Animator>();
    public List<Color> BallColors = new List<Color>();
     List<Vector3> PeopleStartPos = new List<Vector3>();

    void Start(){
        foreach (var human in People){
            PeopleStartPos.Add(human.transform.position);
        }
        SpawnBalls(BallSize);
        Pool.GetComponent<Pool>().Scene = this;
    }

    public void Clap(){
        foreach (var anim in People)
        {
            anim.SetTrigger("Clap");
        }
        Invoke("SitDown",3.5f);
    }

    public void SitDown(){
        for (int i = 0; i < People.Count; i++)
        {
           People[i].transform.DOMove(PeopleStartPos[i],0.5f); 
        }

    }

    void SpawnBalls(float size){
        // Vector3 zeropos = new Vector3(-2.85f,-0.8f,-33.5f);
        Vector3 zeropos = Pool.position+new Vector3(-5+0.5f*size,0.5f*size,-5+0.5f*size);
        
        
        int m = (int) Mathf.Round(PoolSize.x/size);
        int n = (int) Mathf.Round(PoolSize.y/size);

        for (int i = 0; i < m; i++)
        {
            for (int j = 0; j < n; j++)
            {
                Vector3 pos = new Vector3(zeropos.x+i*size,zeropos.y,zeropos.z+j*size);
                var ball = Instantiate(Ball,pos,Quaternion.identity,Pool);
                ball.transform.localScale = Vector3.one*size;
                ball.GetComponent<Renderer>().material.color = RndColor();
                // Debug.Log(zeropos + "  "+ pos);
            }
        }
    }

    public void Plop(Vector3 pos, bool isopen){
        int n;
        if(isopen) n = Random.Range(3,6);
        else n = Random.Range(15,20);

        float maxforce = 10;
        float lifetime = 5;
        // Debug.Log("Plop "+n);
        GameObject[] balls = new GameObject[n];

        for (int i = 0; i < n; i++)
        {
            var ball = Instantiate(PhysBall,pos+Vector3.up*0.1f,Quaternion.identity);
            ball.GetComponent<Rigidbody>().velocity = Vector3.up*6f;
            // ball.GetComponent<Rigidbody>().AddExplosionForce(2,pos-1*Vector3.down,2);
            var force = new Vector3(Random.Range(-maxforce,maxforce),Random.Range(0,maxforce),Random.Range(-maxforce,maxforce));
            ball.GetComponent<Rigidbody>().AddForce(force);
            
            ball.GetComponent<Renderer>().material.color = RndColor();
            Destroy(ball,lifetime);
        }

    }

    

    Color RndColor(){
        int i = Random.Range(0,BallColors.Count);
        return BallColors[i];
    }
}
