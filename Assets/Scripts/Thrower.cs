﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
// using DG.Tweening;

public class Thrower : MonoBehaviour
{   
    public float StartSpeed;
     public float StartAngularSpeed;
    public float BallRadius;
    public float RoadWidth ;
    public float RoadLength ;
    public LineRenderer Line;
    public Vector2 SafeZone;
    public float MaxThrowForce;
    Vector3 Direction;
    Rigidbody rb;
    StickmanControl SC;
    Vector3 Force;
    Vector3 OnWidth;
    bool start;
    bool push;
    public bool stopcam;
    Vector2 mpos = new Vector2(-1,-1);
    Vector3 NormSafeZone;
    float prevpos = 0;
    
    void OnEnable(){
        StickmanControl.OnReachSkPos += OnReachSkPos;
    }
    void Start()
    {
        rb = GetComponent<Rigidbody>();
        SC = GetComponent<StickmanControl>(); 
        start = false;
        push = false;
        stopcam =false;
        NormSafeZone = SafeZone*Screen.height/1920;
        SpawnLine();
        // mpos= new Vector2(-1,1,);
    }

    void Update()
    {
        if(Input.GetMouseButton(0)&&!start){
            if(mpos.x ==-1&&!MouseInZone())return;
            
            mpos.x = (Input.mousePosition.x)*RoadWidth/Screen.width;
            if(MouseInZone()){
             mpos.y = (Input.mousePosition.y)*RoadLength/(Screen.height);
            }
            
            Direction = new Vector3(mpos.x-RoadWidth*0.5f,0,-RoadLength+mpos.y);
            Line.SetPosition(1,Direction);
        }

        if(Input.GetMouseButtonUp(0)&&!start){
            rb.isKinematic = false;
            rb.velocity = StartSpeed*(NormDirection()+Vector3.up*BallRadius);
            rb.angularVelocity=Vector3.left*StartAngularSpeed;
            Debug.Log(Direction);
            // rb.AddForce(StartSpeed*(Direction+Vector3.up*BallRadius));
            // Debug.Log(Direction);
            start = true;
        }

        if(Input.GetMouseButtonDown(0)&&start&&!push&&!MouseInTopSafeZone()){
            SC.ToggleRagdoll(true);
            push = true;
        }


    }
    
    void FixedUpdate(){
        if(start&&!stopcam){
            CamFollow();
        }
    }
    void OnReachSkPos(){
        stopcam = true;
    }
    public void RestartMan(){
        start = false;
        push = false;

        Line.SetPosition(1,Vector3.zero);

        SC.ToggleRagdoll(false);

        transform.position = Vector3.zero;
	}

    public Vector3 GetDirection(){
        return Direction;
    }

    bool MouseInZone(){

        return Input.mousePosition.y < Screen.height-NormSafeZone.x && Input.mousePosition.y > NormSafeZone.y; 
    }
    bool MouseInTopSafeZone(){
    return Input.mousePosition.y > Screen.height-NormSafeZone.x;
    }   
    Vector3 NormDirection(){
        Vector3 norm = Direction.normalized*(Direction.magnitude/RoadLength*MaxThrowForce);
        return norm;
    }

    void CamFollow(){
        Transform cam = Camera.main.transform;
        // var curpos = SC.pelvis.position.z;
        // var delta = curpos - prevpos;
        var delta = 0.9f*rb.velocity.z*Time.fixedDeltaTime;
        cam.position+=Vector3.forward*delta;
        // prevpos = curpos;
    }

    void SpawnLine(){
        var LineGO = Instantiate(Line.gameObject,Vector3.zero,Quaternion.identity,transform.parent);
        Line = LineGO.GetComponent<LineRenderer>();
        Line.SetPosition(0,Vector3.zero);
        Line.SetPosition(1,Vector3.zero);
    }

    // void OnCollisionEnter(Collision col){
    //     if(col.gameObject.tag=="toDestroy"){
    //         stopcam = true;
    //     }
    // }
}
