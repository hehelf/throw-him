﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
//using Lean.Touch;
using DG.Tweening;
// [ExecuteInEditMode]
public class StickmanControl : MonoBehaviour {

	public float PushForce;
	public float PushVelocity;
	public Animator animator;

	List<Rigidbody> Ragdol = new List<Rigidbody>();

	Vector3 ManStartPos = new Vector3(0,-0.25f,0);
	Vector3[] CurrentPose = new Vector3[10];

	public Transform pelvis;
	float SkDist = 15;

	public delegate void SMevent();

    public static event SMevent OnReachSkPos;
	bool criticalpos = false;
	


	void Start () {
		animator = GetComponent<Animator>();

		foreach (var rb in GetComponentsInChildren<Rigidbody>())
		{
			if (rb.gameObject.name != "Stickman"){
			Ragdol.Add(rb);
			rb.isKinematic=true;
			rb.GetComponent<Collider>().enabled = false;
			}
			if (rb.gameObject.name == "pelvis"){
				pelvis = rb.transform;
			}
		}

	}
	void Update(){
		if(!criticalpos&&-pelvis.position.z>= SkDist){
            criticalpos = true;
			if(OnReachSkPos != null){
				OnReachSkPos();
			}
        }
	 }


	public void ToggleRagdoll(bool active){
		GetComponent<Collider>().enabled = !active;
		GetComponent<Rigidbody>().isKinematic = active;
		animator.enabled = !active;

		// transform.eulerAngles = new Vector3(0,60,-80);
		foreach (var rb in Ragdol)
		{

			rb.isKinematic=!active;
			rb.useGravity=active;
			rb.GetComponent<Collider>().enabled = active;
			if (active){

				// float fact = Random.Range(1f,2f);
				// rb.velocity = new Vector3(0,-0.5f*PushVelocity*fact,PushVelocity*fact);
				var dir = GetComponent<Thrower>().GetDirection().normalized;
				// Debug.DrawLine(transform.position,transform.position-dir*0.5f,Color.red,10);
				// rb.AddExplosionForce(PushForce,transform.position-dir*0.5f,10);
				rb.AddForce((dir+Vector3.up*0.2f)*PushForce);
				// rb.velocity = (dir+Vector3.up*0.3f)*PushVelocity;
				
				if(rb.gameObject.name =="pelvis"||rb.gameObject.name =="spine_03"||rb.gameObject.name =="head"){
					rb.transform.eulerAngles = new Vector3(0,30,-80);
					// rb.transform.DORotate(new Vector3(0,60,-80),0.1f);
				}
			}
		}

	}

	public void StartDance(){
		var pos = new Vector3(pelvis.position.x, 0 , pelvis.position.z);
		transform.position = pos;
		transform.eulerAngles=Vector3.zero;
		animator.enabled = true;
		animator.applyRootMotion = true;
		animator.SetTrigger("Dance");
		if(transform.position.z<-25){
            transform.DOMove(new Vector3(0,0,-23),1);
        }

	}

	

}
