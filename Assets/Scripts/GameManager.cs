﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
using System.Globalization;
 using DG.Tweening;
using GameAnalyticsSDK;

public class GameManager : MonoBehaviour
{
    public int lvl;
    public Transform Container;
    public GameObject stickman;
    public GameObject StickmanPrefab;
    public GameObject SkittlePrefab;
    public float SkittleDist;
    public List<Transform> CamPos = new List<Transform>();
    public List<GameObject> lvlSkittles = new List<GameObject>();
    public SceneControl Scene;

    public List<GameObject> Buttons = new List<GameObject>();
    float skMass = 0.2f;
    public static int StrikedCnt;
     int TolalSkittles;
     bool criticalpos;
    

    GameObject ThisMap;
    
    void Awake(){
        GameAnalytics.Initialize();
    }
 
    // Start is called before the first frame update
    void Start()
    {
        SpawnAll();
    }

    void Update(){
        if(StrikedCnt==TolalSkittles){
            StrikedCnt++;// чтобы срабатывала 1раз
            StartCoroutine(Win());
            
        }

    }

    void Fail(){
        if(StrikedCnt>=TolalSkittles)return;
        Buttons[1].SetActive(true);
    }

    void OnReachSkPos(){
        Invoke("Fail",2);
    }
    public IEnumerator Win(){
        GameAnalytics.NewProgressionEvent(GAProgressionStatus.Complete, $"Complete level");
        SetCamPos(2,1);
        // var anim = stickman.GetComponent<Animator>();
        yield return new WaitForSeconds(1);
        stickman.GetComponent<StickmanControl>().StartDance();

        // anim.SetTrigger("Dance");
        Scene.Clap();
        yield return new WaitForSeconds(2);
        Buttons[0].SetActive(true);

    }
    public void Restart(){
        // StartCoroutine(RestartCorutine());
                ClearContainer();
        // yield return new WaitForEndOfFrame();
        SpawnAll();
        SetCamPos(1);
        HideBtns();
    }
    // IEnumerator RestartCorutine()
    // {
    //     // stickman.GetComponent<Thrower>().RestartMan();
    //     ClearContainer();
    //     yield return new WaitForEndOfFrame();
    //     SpawnSkittles(SkittleCount);
    //     SpawnSticman();
    //     SetCamPos(1);


    // }

    void HideBtns(){
        Buttons[0].SetActive(false);
        Buttons[1].SetActive(false);
    }
    public void Nextlvl(){
        ClearContainer();
        IncMap();
        SpawnAll();
        SetCamPos(1);
       HideBtns();
    }
    // IEnumerator NextlvlCourutine(){
    //     RemoveAll();
    //     // yield return new WaitForFixedUpdate();
    //     IncMap();
    //     SpawnAll();
    //     SetCamPos(1);
    //     yield return new WaitForFixedUpdate();
    // }

    void IncMap(){
        lvl++;
        if (lvl >= lvlSkittles.Count){
            lvl = 0;
        }
    }

    void SpawnSticman(){
        stickman =Instantiate(StickmanPrefab,Vector3.zero,Quaternion.identity,Container);
        stickman.name = "Stickman";
        StickmanControl.OnReachSkPos += OnReachSkPos;
    }

    void SpawnSkittles(){
        var skpos= Vector3.back*SkittleDist;
        var sk = Instantiate(lvlSkittles[lvl],skpos,Quaternion.identity,Container);

        var skarr = sk.GetComponentsInChildren<Rigidbody>();
        TolalSkittles = skarr.Length;
        StrikedCnt = 0;

            foreach (var rb in skarr)
            {
                rb.mass=skMass;
            }    
    }
    
    void SetCamPos(int pos){
        int i = pos-1;
        Camera.main.transform.position = CamPos[i].position;
        Camera.main.transform.rotation = CamPos[i].rotation;
        
    }
    void SetCamPos(int pos,float dur){
        int i = pos-1;
        Camera.main.transform.DOMove(CamPos[i].position,dur);
        Camera.main.transform.DORotateQuaternion(CamPos[i].rotation,dur);
        
    }

    void ClearContainer(){
        for (int i = 0; i < Container.childCount; i++)
        {
            Destroy(Container.GetChild(i).gameObject);
        }
        criticalpos = false;
    }

    // void SpawnMap(int i){
    //     ThisMap  = Instantiate(mapPrefab[map],Vector3.zero,Quaternion.identity);
    //     var MI = ThisMap.GetComponent<MapInfo>();

    // }

    void SpawnAll(){
        // SpawnMap(map);
        SpawnSkittles();
        SpawnSticman();

    }



    

    
}
