﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Skittle : MonoBehaviour
{
    public bool activesk = true;
    Vector3 vup;
     bool striked = false;
    void Start()
    {
        // rb = GetComponent<Rigidbody>();
        
        // transform.eulerAngles;
        if (!activesk) GameManager.StrikedCnt++;
    }

    // Update is called once per frame
    void Update()
    {   
        if (!activesk) return;
        // var eul =transform.eulerAngles;
        vup =transform.up;
        var thisang = Vector3.Angle(vup,Vector3.up);
        if(thisang>30&&!striked){
            striked =true;
            GameManager.StrikedCnt++;
            Debug.Log("str"+transform.GetSiblingIndex()+" // "+GameManager.StrikedCnt);
        }
    }
}
