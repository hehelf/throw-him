﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Pool : MonoBehaviour
{
     public  SceneControl Scene;
    void OnTriggerEnter(Collider col){
        if (col.gameObject.tag == "Ragdoll"||col.gameObject.tag == "Player"||col.gameObject.tag == "toDestroy")
        {   
            // Debug.Log("trigg");
            var collisionPoint = col.transform.position;
            Scene.Plop(collisionPoint,col.gameObject.tag!="Player");
        }
        if(col.gameObject.tag == "Player"){
            col.enabled=false;
        }
    }   

}
